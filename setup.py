#!/usr/bin/env python

import distutils.command.build_py
import glob
import os
import sys

# NOTE: distutils takes care of pathname separators, expecting everything
# to use slashes.  But when glob is used, os.path.join must be used too.

PACKAGE_DIR = 'src/FontFinder'
UI_FILES = glob.glob(os.path.join('src', 'ui', '*.ui'))

class build_py(distutils.command.build_py.build_py):
    """Modified build_py class"""
    def build_module (self, module, module_file, package):
        """
        Insert real package installation locations into the build_config.py module.

        Inspired by a number of sources, mostly meld.
        """
        # Get the install prefix
        try:
            iobj = self.distribution.command_obj['install']
            prefix = iobj.prefix
        except KeyError as e:
            print (e)
            prefix = sys.prefix
        # Specify the path to the ui files
        ui_dir = os.path.join(prefix, 'share', 'FontFinder', 'ui')
        graphics_dir = os.path.join(prefix, 'share', 'pixmaps')

        # Modify the build_config.py file that contains this path definition
        if module_file == os.path.join(PACKAGE_DIR, 'build_config.py'):
            # Read in build_config.py
            with open(module_file) as mf:
                lines = mf.read().splitlines()

            # Find the line starting 'UI_DIR = ' and overwrite it
            for i, line in enumerate(lines):
                if line.startswith("UI_DIR = "):
                    lines[i] = "UI_DIR = '%s'" % (ui_dir)
                if line.startswith("GRAPHICS_DIR = "):
                    lines[i] = "GRAPHICS_DIR = '%s'" % (graphics_dir)

            # Write the file out again
            with open(module_file, 'w') as mf:
                mf.write("\n".join(lines))
        # Execute the original build_module() method.
        return distutils.command.build_py.build_py.build_module(self, module, module_file, package)

distutils.core.setup(name='fontfinder',
    version='0.2',
    description="Font Finder - Find the font you're searching for",
    url='http://jared.henley.id.au/software/fontfinder',
    author='Jared Henley',
    author_email='jaredhenley@gmail.com',
    packages=['FontFinder'],
    package_dir={'FontFinder': PACKAGE_DIR},
    data_files=[('share/FontFinder/ui', UI_FILES),
        ('share/doc/FontFinder', ['src/COPYING',]),
        ('share/pixmaps', ['src/fontfinder.svg',]),
        ('share/applications', ['src/fontfinder.desktop',]),
        ],
    scripts=['src/fontfinder'],
    cmdclass={"build_py": build_py
    }
    )
