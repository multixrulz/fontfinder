#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtGui, QtCore

class FFItem(QtGui.QStandardItem):
    """
    A subclassed QStandardItem that allows for sane handling of non-string
    items.

    Trees are allowed, as long as you send in both rows and columns as arguments,
    and not rows only.
    """
    def __init__(self, data, *args, **kwargs):
        """
        Create the model.

        data: the data to be displayed by the item.
        """
        # QStandardItem has two function signatures with a single argument:
        # one is a string, the other is an integer for creating trees.
        # The second possibility is ignored.
        if len(args) == 0:
            super().__init__(None, *args, **kwargs)
            self.set_item_data(data)
        else:
            super().__init__(data, *args, **kwargs)
        self.setEditable(False)

    def original_data(self):
        """
        Get the unmodified data sent in through the constructor or the
        set_item_data() method.
        """
        return self._data

    def set_item_data(self, data):
        """
        Set the data to be displayed by the item.

        data: the data to be displayed.
            QPixmap or QIcon: displayed as an image.
            datetime.date: displayed as a string
            decimal.Decimal: Qt doesn't handle this by default.
                displayed with at least 2 decimal places, more if there are
                non-zero digits.
            boolean: displayed as a checkbox.
        """
        self._data = data
        if data == None:
            display = ''
        elif isinstance(data, list):
            display = ", ".join([str(d) for d in data])
        elif isinstance(data, bool):
            self.setCheckable(False) # Makes a checkbox show up
            if data:
                self.setCheckState(QtCore.Qt.Checked)
            else:
                self.setCheckState(QtCore.Qt.Unchecked)
            display = ''
        else:
            display = str(data)
        # Set the DisplayRole data
        self.setData(display, QtCore.Qt.DisplayRole)

    def __lt__(self, other):
        # Define our own less-than method: this is what Qt uses for sorting
        try:
            result = self._data < other._data
        except TypeError:
            # If the types are unorderable, just return True
            result = True
        return result


class FFFilterProxyModel(QtCore.QSortFilterProxyModel):
    def __init__(self, *args, **kwargs):
        """
        Create the proxy.
        """
        self._name = ''
        self._writing_systems = set()
        self._weights = set()
        self._num_weights = 1
        self._italic = False
        self._monospaced = False
        self._scalable = True
        super().__init__(*args, **kwargs)

    def filterAcceptsRow(self, sourcerow, parent):
        # Get the details of the font
        family = self.sourceModel().item(sourcerow, 0).original_data()
        weights = self.sourceModel().item(sourcerow, 1).original_data()
        italic = self.sourceModel().item(sourcerow, 2).original_data()
        monospaced = self.sourceModel().item(sourcerow, 3).original_data()
        scalable = self.sourceModel().item(sourcerow, 4).original_data()
        writing_systems = self.sourceModel().item(sourcerow, 5).original_data()

        # Look for individual words in the name filter, see if they exist
        # as substrings in the font family name
        family = family.lower()
        names = self._name.lower().split(' ')
        if len(names) == 0:
            name_ok = True
        else:
            name_ok = all([family.count(n) > 0 for n in names])

        # Check the required number of weights exists
        num_weights_ok = len(weights) >= self._num_weights

        # See if there's any overlap between the desired weights and the actual
        if len(self._weights) == 0:
            weight_ok = True
        else:
            weight_ok = any(
                [any([self._check_weight(w, w2)
                        for w2 in self._weights])
                    for w in weights]
                )

        # Check that italic exists if required
        if self._italic == False:
            italic_ok = True
        else:
            italic_ok = italic

        # Check that font is monospaced as required
        monospaced_ok = monospaced == self._monospaced

        # Check that font is scalable as required
        scalable_ok = scalable == self._scalable

        # Check that the desired writing systems exist
        if len(self._writing_systems) == 0:
            writing_systems_ok = True
        else:
            writing_systems_ok = all([ws in writing_systems for ws in self._writing_systems])

        return all([name_ok, num_weights_ok, weight_ok, italic_ok, monospaced_ok,
            scalable_ok, writing_systems_ok])

    def _check_weight(self, weight, weight_range):
        return weight >= weight_range[0] and weight <= weight_range[1]

    def set_name(self, name):
        self._name = name
        self.invalidateFilter()

    def set_writing_systems(self, writing_systems):
        self._writing_systems = set(writing_systems)
        self.invalidateFilter()

    def set_weights(self, weights):
        self._weights = set(weights)
        self.invalidateFilter()

    def set_num_weights(self, num_weights):
        self._num_weights = num_weights
        self.invalidateFilter()

    def set_italic(self, italic):
        self._italic = italic
        self.invalidateFilter()

    def set_monospaced(self, monospaced):
        self._monospaced = monospaced
        self.invalidateFilter()

    def set_scalable(self, scalable):
        self._scalable = scalable
        self.invalidateFilter()

    def __lt__(self, other):
        # Define our own less-than method: this is what Qt uses for sorting
        try:
            result = self._data < other._data
        except TypeError:
            # If the types are unorderable, just return True
            result = True
        return result

    def lessThan(self, leftIndex, rightIndex):
        """
        Re-implemented method that is used for sorting.

        leftIndex: the index of the item on the left-hand side of the < operator.
        rightIndex: the index of the item on the right-hand side of the < operator.
        """
        left_data = self.sourceModel().itemFromIndex(leftIndex).original_data()
        right_data = self.sourceModel().itemFromIndex(rightIndex).original_data()
        return (left_data < right_data)
