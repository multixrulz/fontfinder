#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# TODO: Fontconfig extracts a bit more info that might be handy
# http://www.freedesktop.org/software/fontconfig/fontconfig-devel/x19.html
# Fields of interest:
#   slant
#   width
#   decorative

from PyQt5 import QtWidgets, QtGui, QtCore, uic
import sys
import os
import traceback

from . import build_config
from .qt_extensions import *

class FFMainWindow(QtWidgets.QMainWindow):
    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app

        # Set a custom exception hook in order to display a nice message
        # dialog when an exception occurs.
        sys.excepthook = self._custom_exception_hook

        super().__init__(*args, **kwargs)

        self._init_gui()
        self._init_data()

        sm = self.treeView_font.selectionModel()
        sm.selectionChanged.connect(self._show_selected_font)
        self.show()

    def _init_gui(self):
        uic.loadUi(os.path.join(build_config.UI_DIR, "ff_main_window.ui"), self)
        self._font_model = QtGui.QStandardItemModel()
        self._font_filter = FFFilterProxyModel()
        self._font_filter.setSourceModel(self._font_model)
        self.treeView_font.setModel(self._font_filter)

    def _init_data(self):
        database = QtGui.QFontDatabase()

        for family in database.families():
            styles = database.styles(family)
            if len(styles) > -1:
                weights = set([database.weight(family, s) for s in styles])
                italics = any([database.italic(family, s) for s in styles])
                monospaced = any([database.isFixedPitch(family, s) for s in styles])
                scalable = any([database.isScalable(family, s) for s in styles])
                writing_systems_int = database.writingSystems(family)
                writing_systems = [database.writingSystemName(ws) for ws in writing_systems_int]
                row = [FFItem(family),
                    FFItem(weights),
                    FFItem(italics),
                    FFItem(monospaced),
                    FFItem(scalable),
                    FFItem(writing_systems_int),
                    FFItem(writing_systems),
                    ]
                self._font_model.appendRow(row)
        headings = ["Font Family", "Weights", "Italic", "Monospaced",
            "Scalable", "Writing Systems (int)", "Writing Systems"]
        self._font_model.setHorizontalHeaderLabels(headings)
        # Hide some columns
        self.treeView_font.setColumnHidden(1, True) # Weights
        self.treeView_font.setColumnHidden(2, True) # Italic
        self.treeView_font.setColumnHidden(3, True) # Monospaced
        self.treeView_font.setColumnHidden(4, True) # Scalable
        self.treeView_font.setColumnHidden(5, True) # Writing Systems (int)
        # Set auto column widths
        self.treeView_font.resizeColumnToContents(0)

    def _show_selected_font(self):
        self.textEdit.clear()

        sm = self.treeView_font.selectionModel()
        selected_rows = sm.selectedRows(0)
        if len(selected_rows) > 0:
            familyItem = sm.selectedRows(0)[0]
            family = familyItem.data()
            self._show_font(family)

    def _show_font(self, family):
        database = QtGui.QFontDatabase()
        styles = database.styles(family)
        styles.sort()

        for style in styles:
            # Show the font's data in boring text
            block_format = QtGui.QTextBlockFormat()
            block_format.setAlignment(QtCore.Qt.AlignLeft)
            font = QtGui.QFont()

            self.textEdit.setCurrentFont(font)
            cursor = self.textEdit.textCursor()
            cursor.insertBlock(block_format)
            cursor.insertText("%s %s" % (family, style))

            # Show a sample in the font
            size = self.spinBox_size.value()
            weight = database.weight(family, style)
            italic = database.italic(family, style)

            block_format = QtGui.QTextBlockFormat()
            block_format.setAlignment(QtCore.Qt.AlignLeft)
            block_format.setIndent(1)
            font = QtGui.QFont(family, size, weight, italic)

            self.textEdit.setCurrentFont(font)
            cursor = self.textEdit.textCursor()
            cursor.insertBlock(block_format)
            sample_text = self.lineEdit_sample.text()
            if sample_text == '':
                samples = [database.writingSystemSample(ws) for ws in database.writingSystems(family)]
                cursor.insertText("\n".join(samples))
            else:
                cursor.insertText(sample_text)

    def _name_changed(self):
        self._font_filter.set_name(self.lineEdit_name.text())

    def _num_weights_changed(self):
        self._font_filter.set_num_weights(self.spinBox_weights.value())

    def _weight_toggled(self):
        weights = []
        if self.cb_wt_ultralight.isChecked():
            weights.append((0,25))
        if self.cb_wt_light.isChecked():
            weights.append((25,45))
        if self.cb_wt_medium.isChecked():
            weights.append((45,55))
        if self.cb_wt_black.isChecked():
            weights.append((55,70))
        if self.cb_wt_extrablack.isChecked():
            weights.append((70,85))
        if self.cb_wt_ultrablack.isChecked():
            weights.append((85,100))
        self._font_filter.set_weights(weights)

    def _italic_toggled(self):
        self._font_filter.set_italic(self.cb_italic.isChecked())

    def _monospaced_toggled(self):
        self._font_filter.set_monospaced(self.cb_monospaced.isChecked())

    def _scalable_toggled(self):
        self._font_filter.set_scalable(self.cb_scalable.isChecked())

    def _writing_system_toggled(self):
        writing_systems = []
        if self.cb_ws_latin.isChecked():
            writing_systems.append(1)
        if self.cb_ws_greek.isChecked():
            writing_systems.append(2)
        if self.cb_ws_cyrillic.isChecked():
            writing_systems.append(3)
        if self.cb_ws_armenian.isChecked():
            writing_systems.append(4)
        if self.cb_ws_hebrew.isChecked():
            writing_systems.append(5)
        if self.cb_ws_arabic.isChecked():
            writing_systems.append(6)
        if self.cb_ws_syriac.isChecked():
            writing_systems.append(7)
        if self.cb_ws_thaana.isChecked():
            writing_systems.append(8)
        if self.cb_ws_devanagari.isChecked():
            writing_systems.append(9)
        if self.cb_ws_bengali.isChecked():
            writing_systems.append(10)
        if self.cb_ws_gurmukhi.isChecked():
            writing_systems.append(11)
        if self.cb_ws_gujarati.isChecked():
            writing_systems.append(12)
        if self.cb_ws_oriya.isChecked():
            writing_systems.append(13)
        if self.cb_ws_tamil.isChecked():
            writing_systems.append(14)
        if self.cb_ws_telagu.isChecked():
            writing_systems.append(15)
        if self.cb_ws_kannada.isChecked():
            writing_systems.append(16)
        if self.cb_ws_malayam.isChecked():
            writing_systems.append(17)
        if self.cb_ws_sinhala.isChecked():
            writing_systems.append(18)
        if self.cb_ws_thai.isChecked():
            writing_systems.append(19)
        if self.cb_ws_lao.isChecked():
            writing_systems.append(20)
        if self.cb_ws_tibetan.isChecked():
            writing_systems.append(21)
        if self.cb_ws_myanmar.isChecked():
            writing_systems.append(22)
        if self.cb_ws_georgian.isChecked():
            writing_systems.append(23)
        if self.cb_ws_khmer.isChecked():
            writing_systems.append(24)
        if self.cb_ws_simplified_chinese.isChecked():
            writing_systems.append(25)
        if self.cb_ws_traditional_chinese.isChecked():
            writing_systems.append(26)
        if self.cb_ws_japanese.isChecked():
            writing_systems.append(27)
        if self.cb_ws_korean.isChecked():
            writing_systems.append(28)
        if self.cb_ws_vietnamese.isChecked():
            writing_systems.append(29)
        if self.cb_ws_symbol_other.isChecked():
            writing_systems.append(30)

        self._font_filter.set_writing_systems(writing_systems)


    def _custom_exception_hook(self, type, value, actual_traceback):
        ### Define a customised sys.excepthook to display exceptions nicely to the user
        traceback_string = "".join(traceback.format_exception(type, value, actual_traceback))
        sys.stderr.write(traceback_string)
        sys.stderr.flush()
        # TODO: TRANSLATE
        # Split out some different errors
        primary_text = "Oops! We seem to have hit a bug."
        secondary_text = "There seems to be a programming error in Font Finder.  The text below will be helpful to the developers.  This is Font Finder version 0.2."
        exit_app = True

        # Display the error in a message box
        msgbox = QtWidgets.QMessageBox()
        msgbox.setWindowTitle("Font Finder Error")
        msgbox.setIcon(QtWidgets.QMessageBox.Critical)
        msgbox.setText("<h1>%s</h1>%s" % (primary_text, secondary_text))
        msgbox.setDetailedText(traceback_string)
        msgbox.setStandardButtons(QtWidgets.QMessageBox.Close)
        msgbox.setDefaultButton(QtWidgets.QMessageBox.Close)
        result = msgbox.exec()
        if exit_app:
            self.app.quit()


def exec():
    """
    Execute the FontFinder program.

    Simply import this module and execute this exec() function.
    """
    # Create an application object
    app = QtWidgets.QApplication(sys.argv)

    # Set an icon for the application
    app_icon = QtGui.QIcon(os.path.join(build_config.GRAPHICS_DIR, 'fontfinder.svg'))
    app.setWindowIcon(app_icon)

    # Create and show the main window -- the window shows itself
    main_window = FFMainWindow(app)

    # Execute the application -- get the event loop running
    return app.exec()

    app.exec()

