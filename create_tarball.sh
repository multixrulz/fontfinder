#!/bin/bash

echo "Setting correct permissions on ui files"
chmod 644 src/ui/*.ui
echo "Setting correct permissions on other files"
chmod 644 src/fontfinder.svg
chmod 644 src/fontfinder.desktop
python setup.py sdist
